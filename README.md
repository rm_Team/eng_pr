1) Установи MongoDB на компутир
  На [офф сайте](https://www.mongodb.com) есть инструкция, все по ней сделай
  
2) Установи все зависимости, если надо
Советую прописать npm install -g mongod

3) В отдельной вкладке/окне терминала пропиши mongod,
не закрывай это

4) В отдельной вкладке/окне терминала пропиши webpack --watch

5) В новой вкладке или окне пропиши npm run start
  Готово.

6) Приготовить production билд:
   * webpack -p
   * Переместить папку app из server в корень
   * npm run build
   * Вернуть app обратно
